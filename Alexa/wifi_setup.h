#include <Arduino.h>
#include <ESP8266WiFi.h>

extern String wifi_setup(String wifi_ssid, String wifi_pass)
{
    WiFi.mode(WIFI_STA);
    Serial.printf("[WIFI] connection to %s", wifi_ssid.c_str());
    WiFi.begin(wifi_ssid.c_str(), wifi_pass.c_str());
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print('.');
        delay(100);
    }
    Serial.println();
    Serial.printf("[WiFi] STATION Mode, SSID: %s, IP address: %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
    return WiFi.localIP().toString();
}