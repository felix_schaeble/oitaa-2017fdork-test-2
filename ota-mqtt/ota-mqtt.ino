#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>
#include <SimpleDHT.h>

const char* version = "v1.0";

const char* ssid = "***";
const char* pass = "***";
const char* topic = "***";
const char* clientName = "***";
const char* ip = "***"; // Mqtt Broker IP-Adresse

void callback(char* topic, byte* payload, unsigned int length){

}

WiFiClient wifiClient;
PubSubClient client(ip, 1883, callback, wifiClient);

SimpleDHT11 dht11;

void publish(String message){
    client.publish(topic, (String(clientName) + " " + message).c_str());
}

void setup() {
    WiFi.begin(ssid, pass);

    while (millis() < 30000 && WiFi.status() != WL_CONNECTED)
        delay(500);

    if(client.connect((char*)clientName))
        publish("connected: " + WiFi.localIP().toString());
    else
        ESP.restart();

    //ArduinoOTA.setHostname((char*)clientName);
    ArduinoOTA.setPassword("0815");
    
    // Arduino OTA stoppt alle WiFi-Verbindungen und ruft danach die Callbacks auf.
    // ArduinoOTA.onStart([]() {
    // interrupts etc beenden
    // });

    ArduinoOTA.begin();
    
    publish("firmware: " + (String)version);
    publish("startup time: " + (String)millis());
}

void loop() {
    ArduinoOTA.handle();
}
